import { defineStore } from "pinia"

export const useExample = defineStore({
    id: "example",
    state: () => {
        return {
        }
    },
    getters: {
    },
    actions: {
    }
})